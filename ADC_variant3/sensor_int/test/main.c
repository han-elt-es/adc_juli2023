#include <stdio.h>
#include "../app/sensorFunctions.h"

#include "Unity/unity_internals.h"
#include "Unity/unity.h"


void setUp(void)
{

}

void tearDown(void)
{

}

void test_fillSensor(void)
{
    printf("---- fill sensor()\n");

    {
        t_sensor s1 = {0, NULL};
        int testId = 5;
        int testData[] = {1, 2, 3, 4};
        double testLength = sizeof(testData)/4;

        fillSensor(&s1, testId, testData, testLength);

	TEST_ASSERT_EQUAL_INT_ARRAY(testData, s1.data, testLength);
	TEST_ASSERT_EQUAL_INT(testId, s1.id);

        // Add your UNITY testing here        
    }
}

void test_copySensor(void)
{
    printf("---- copy sensor()\n");

    {
        t_sensor s1 = {0, NULL};
        t_sensor s2 = {0, NULL};
        int testId = 5;
        int testData[] = {1, 2, 3, 4};
        double testLength = sizeof(testData)/4;

        fillSensor(&s1, testId, testData, testLength);

	TEST_ASSERT_EQUAL_INT_ARRAY_MESSAGE(testData, s1.data, testLength, "S1 Filled with testdata");
	TEST_ASSERT_EQUAL_INT_MESSAGE(testId, s1.id, "S1 Filled with testdata");

        copySensor(&s2,s1);
	TEST_ASSERT_EQUAL_INT_ARRAY_MESSAGE(s1.data, s2.data, testLength, "S1 S2 compared");
	TEST_ASSERT_EQUAL_INT_MESSAGE(s1.id, s2.id, "S1 S2 compared");

        // Add your UNITY testing here        
    }

    {
	// Add other testcases here, don't forget to test boundaries        
    }
}

int main(void)
{
    UNITY_BEGIN();
    printf("== Unit tests 'complex calculation library' ==\n\n");
    RUN_TEST(test_fillSensor);
    RUN_TEST(test_copySensor);
    return UNITY_END();
}