#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sensorFunctions.h"

int main(int argc, char *argv[])
{
    t_sensor s2;
    t_sensor s1;
    int data[] = {1, 2, 3, 6};
    fillSensor(&s1, 2, data, 4);
    printSensor(s1);

    copySensor(&s2, s1);
    printSensor(s2);

    return 0;
}