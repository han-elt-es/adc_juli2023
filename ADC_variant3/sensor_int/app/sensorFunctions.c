/* 
* Code for Advance C
* a library to manage sensor structures
* 
* HAN University of applied sciences (c) 2023
* 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sensorFunctions.h"

/*!
*   \brief printSensor()
*   \param t_sensor sensor  sensor to be printed
*
*   Prints the sensor in a neath way
*/
void printSensor(t_sensor sensor)
{

}


/*!
*   \brief fillSensor()         
*   \param t_sensor *sensor 
*   \param id sensorId
*   \param int *data
*
*
*   Fills a sensor with a name (string) and an value (double)
*/
void fillSensor(t_sensor *sensor, int sensorId, int *data, int dataLength)
{
    sensor->data = malloc((dataLength + 1)*4);
}


/*!
*   \brief copySensor()         
*   \param t_sensor *dest 
*   \param char *name
*   \param int length
*   \param double value
*
*   Copies the data from source sensor to destination sensor
*/

void copySensor(t_sensor *dest, const t_sensor source)
{

}
