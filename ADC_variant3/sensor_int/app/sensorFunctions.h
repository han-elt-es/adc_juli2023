#ifndef SENSORFUNCTIONS_H
#define SENSORFUNCTIONS_H
typedef struct sensor
{
    int id;                 //Sensor identification
    int *data;              //Sensor data, dynamic length array of integers terminated with 0
} t_sensor;

void fillSensor(t_sensor *sensor, int sensorId, int *data, int dataLength);
void printSensor(t_sensor sensor);
void copySensor(t_sensor *dest, const t_sensor source);

#endif // SENSORFUNCTIONS_H