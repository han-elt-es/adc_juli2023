#include <stdio.h>
#include <stdlib.h>

#include "../app/sensorFunctions.h"

#include "Unity/unity_internals.h"
#include "Unity/unity.h"


void setUp(void)
{

}

void tearDown(void)
{

}


/// one test is implemented here
/// if your code in the library sensorFunctions is correct
/// this will work without an error
void test_fillSensor(void)
{
    printf("---- fill sensor()\n");

    {
        t_sensor s1 = {NULL, 0};
        char testtext[] = "thequickbrownfoxjumpsoverthelazydog";
        int testlength = sizeof(testtext);
        double testvalue = 3.14;

        fillSensor(&s1,testtext, testlength, testvalue);

	TEST_ASSERT_EQUAL_STRING(testtext, s1.name);
	TEST_ASSERT_EQUAL_DOUBLE(testvalue, s1.value);

    }

    {
	// Add other testcases here, don't forget to test boundaries        
    }
}

///This set of asserts tests the function that removes a sensor
///In fact removing a sensor means that the allocated memory for the sensorname
/// is freed and the sensor name pointer is NULL, the sensor data is 0
/// Only the result in the struc is tested. 
/// You have to check with valgrind if the memory is really freed

void test_removeSensor(void)
{
    printf("---- remove sensor()\n");

    {
        t_sensor s1 = {NULL, 0};
        char testtext[] = "thequickbrownfoxjumpsoverthelazydog";
        int testlength = sizeof(testtext);
        double testvalue = 3.14;
        /// fill the structure s1 with a string and a value including
        /// dynamic memory allocation 
        s1.name = malloc(testlength);
        TEST_ASSERT_NOT_NULL_MESSAGE(s1.name, "Allocation of memory not succeeded, system errotr not a function error");
        s1.value = testvalue;

        removeSensor(&s1);
        TEST_ASSERT_NULL(s1.name);
        TEST_ASSERT_EQUAL_DOUBLE(0, s1.value);

    }

}

int main(void)
{
    UNITY_BEGIN();
    printf("== Unit tests 'sensor Library functions' ==\n\n");
    RUN_TEST(test_fillSensor);
    RUN_TEST(test_removeSensor);
    return UNITY_END();
}
