/* 
* Code for Advance C
* a library to manage sensor structures
* 
* HAN University of applied sciences (c) 2023
* 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sensorFunctions.h"

/*!
*   \brief printSensor()
*   \param t_sensor sensor  sensor to be printed
*
*   Prints the sensor in a neath way
*/
void printSensor(t_sensor sensor)
{

}


/*!
*   \brief fillSensor()         
*   \param t_sensor *sensor 
*   \param char *name
*   \param int length
*   \param double value
*
*   Fills a sensor with a name (string) and an value (double)
*/
void fillSensor(t_sensor *sensor, char *name, int length, double value)
{

}


/*!
*   \brief copySensor()         
*   \param t_sensor *dest 
*   \param char *name
*   \param int length
*   \param double value
*
*   Copies the data from source sensor to destination sensor
*/

void copySensor(t_sensor *dest, const t_sensor source)
{

}

/*!
*   \brief removeSensor()         
*   \param t_sensor *sensor 
*
*   Removes the data of the sensor, 
*   it also frees the dynamicly allocated memory use by 'name' 
*/
void removeSensor(t_sensor *sensor)
{

}

