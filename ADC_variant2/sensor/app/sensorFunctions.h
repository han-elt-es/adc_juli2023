#ifndef SENSORFUNCTIONS_H
#define SENSORFUNCTIONS_H
typedef struct sensor
{
    char *name;
    double value;
} t_sensor;

void printSensor(t_sensor sensor);
void fillSensor(t_sensor *sensor, char *name, int length, double value);
void removeSensor(t_sensor *sensor);

#endif // SENSORFUNCTIONS_H
