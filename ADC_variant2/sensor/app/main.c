/*
*   Main code to demonstrate the wording of the sensorlibrary
*
*   HAN University of applied science 2023   
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sensorFunctions.h"

int main(int argc, char *argv[])
{
    t_sensor s2;
    t_sensor s1;
    t_sensor s0;
    int j = 0;

    char *demoText      = "Test";
    int demoLength      = sizeof(demoText);
    double demoValue    = 3.14;
    
    /// Demonstrate the use of fillsensor

    fillSensor(&s0, "Test", 5, 3.14);
    printSensor(s0);

    /// cleanup
    removeSensor(&s0);

    /// Use the commandline to fill a sensor
    if ((argc > 2))
    {
        /// add the code to fill sensor s1 with the commandline information
    }
    else
    {
        printf("Not enough arguments!\n");
    }

    /// copy the sensor information from s1 to s2
    copySensor(&s2, s1);
    /// demonstrate the copy has worked
    printSensor(s1);
    printSensor(s2);

    return 0;
}