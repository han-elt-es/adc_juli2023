#include <stdio.h>
#include <stdlib.h>

#include "../app/sensorlib.h"

#include "Unity/unity_internals.h"
#include "Unity/unity.h"


void setUp(void)
{

}

void tearDown(void)
{

}


/// one test is implemented here
/// if your code in the library sensorFunctions is correct
/// this will work without an error
void test_fillSensor(void)
{
    printf("---- fill sensor()\n");

    {
        t_sensor s1 = {NULL, 0};
        char testtype[] = "temperature";
        int typelength = sizeof(testtype);
        int testid = 31;
        double testvalue = 3.14;

        fillSensor(&s1,testtype, typelength, testid, testvalue);

	TEST_ASSERT_EQUAL_STRING(testtype, s1.name);
    TEST_ASSERT_EQUAL_INT(testtype, s1.name);
	TEST_ASSERT_EQUAL_DOUBLE(testvalue, s1.value);

    }

    {
	// Add other testcases here, don't forget to test boundaries        
    }
}

///This set of asserts tests the function that removes a sensor
///In fact removing a sensor means that the allocated memory for the sensorname
/// is freed and the sensor name pointer is NULL, the sensor data is 0
/// Only the result in the struc is tested. 
/// You have to check with valgrind if the memory is really freed

void test_compareSensors(void)
{
    printf("---- compare sensor()\n");

    {
        t_sensor s1;
        t_sensor s2;

        char testtype1[] = "temperature";
        int typelength1 = sizeof(testtype1);
        int testid = 31;
        double testvalue = 3.14;

        fillSensor(&s1,testtype, typelength, testid, testvalue);

        char testtype2[] = "pressure";
        int typelength2 = sizeof(testtype1);
        int testid = 32;
        double testvalue = 3.14;

        fillSensor(&s2,testtype2, typelength2, testid2, testvalue2);

    }


    {
        // add more tests with different outcomes.
    }

}

int main(void)
{
    UNITY_BEGIN();
    printf("== Unit tests 'sensor Library functions' ==\n\n");
    RUN_TEST(test_fillSensor);
    RUN_TEST(test_compareSensors);
    return UNITY_END();
}
