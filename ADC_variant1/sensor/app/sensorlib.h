#ifndef SENSORLIB_H
#define SENSORLIB_H

typedef struct sensor
{
  // define the correct members of the t_sensor struct
} t_sensor;


void fillSensor(t_sensor *sensor, char *type, int lengthType, int number, double data);
void printSensor(t_sensor sensor);

int compareSensor(t_sensor s1, t_sensor s2);








#endif // SENSORLIB_H
