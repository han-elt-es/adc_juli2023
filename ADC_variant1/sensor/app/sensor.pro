TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c \
        sensorlib.c

HEADERS += \
    sensorlib.h
